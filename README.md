# ParserZastavokNet
# Парсер сайта https://zastavok.net/

*Исхаков Тимур*

## Описание

Парсер скачивает изображения с сайта zastavok.net и сохраняет их 
в папку wallpapers.

Написан с помощью библиотек:

-  BeautifulSoup

-  Requests.

Ссылки на библиотеки:

-  https://www.crummy.com/software/BeautifulSoup/bs4/doc.ru/bs4ru.html
-  https://pythonru.com/biblioteki/kratkoe-rukovodstvo-po-biblioteke-python-requests
        
