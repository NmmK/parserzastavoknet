import requests
from bs4 import BeautifulSoup
import fake_useragent

image_number = 0
site_number = 2
url = f"https://zastavok.net/"

for storage in range(1):
     # возвращает объект; с методом text возвращает html файл
    response = requests.get(f'{url}/{site_number}').text

    soup = BeautifulSoup(response, 'lxml')
    block = soup.find('div', class_ = 'block-photo')
    all_image = block.find_all('div', class_ = 'short_full')

    for image in all_image:
        image_link = image.find('a').get('href')
        download_storage = requests.get(f'{url}/{image_link}').text
        download_soup = BeautifulSoup(download_storage, 'lxml')
        download_block = download_soup.find('div', class_ = 'image_data').find('div', class_ = 'block_down')
        download_image = download_block.find('a').get('href')
        # Байт код
        image_bytes = requests.get(f'{url}{download_image}').content

        with open(f'wallpapers/{image_number}.jpg', 'wb') as file:
            file.write(image_bytes)

        image_number +=1
        print('Изображение {image_number}.jpg успешно скачано!')
